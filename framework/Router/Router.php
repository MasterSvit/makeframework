<?php
namespace Framework\Router {

    class Router
    {
        public $routers;
        protected $controllername;
        protected $action;
        protected $params = array();

        public function __construct(){}

        /*        public function route($lookup,$pattern)//$pattern)
                {
                    //$lookup - this is a config file
                $c = array (
                    'pattern'    => '',
                    'controller' => '',
                    'action'     => '');
                foreach ($lookup as $element)
                {
                    if ($pattern == $element['pattern']) {$c = $element;}
                }
                return $c;
            }
        */
        public function route($lookup = array(),$pattern)
        {
            //$lookup - this is a config file
            $c = array (
                'pattern'       => '',
                'controller'    => '',
                'action'        => ''
                //,
                //'security'      => '',
                //'_requirements' => array()
            );

            //echo '<p>Pattern = '.$pattern . '</p>';
            foreach ($lookup as $element) {
                if($pattern == $element['pattern']){
                    $c = $element;
                }
            }
                    //try {
                    /*if(!empty($routes[2])){
                        if ($routes[2] == $element['action']) {
                            $this->action = $element['action'];
                            $c['action'] = $element['action'];
                            if(!empty($routes[3])){
                                for ($i = 3; $i <= count($routes), $i < 10; $i++) {
                                    $c['params']= $routes[$i];
                                    print_r($c['params']);
                                    //return $c;
                                }
                            }
                            print '<p> Don\'t set params :';
                            print $c;
                            print '</p>';
                        }
                    //} catch(\Exception $e){
                            //echo $e->getMessage();
                            //else{ $c = $element;}
                    //}
                    }*/

                //else{if('/' == $element['pattern']){ $c = $element; } }
                //}
                //}
            //print_r($c);
            return $c;
        }

        // $params = array('id' => 23)

        public function buildRoute($route_name, $params=array()){
            $url = '';

            if(array_key_exists($route_name, $this->routers)){
                $url = $this->routers[$route_name]['pattern'];

                if(!empty($params)){
                    foreach($params as $key=>$value){
                        $url = str_replace('{'.$key.'}', $value, $url);
                    }
                }

                $url = preg_replace('/\{[\w\d_]+\}/i','',$url);
            }

            return $url;
        }
    }
}