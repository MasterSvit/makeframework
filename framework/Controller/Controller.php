<?php
namespace Framework\Controller {

    use Framework\Model\ActiveRecord;
    use Framework\Request\Request;
    use Framework\Renderer\Renderer;
    use Framework\Response\Response;
    use Framework\Response\ResponseRedirect;
    use Framework\Router\Router;
    use Framework\DI\Service;
    use Blog\Model\Post;
    use Blog\Model\User;

    class Controller
    {
        public $newRenderer;
        public $model;
        public $posts = array();
        protected $l;
        protected $d;
        protected $c;
        protected $p;
        public $data = array();
        public $pathlayout;
        public $post;
        public $user;
        public $router;

        public function __construct()
        {
            //   $this->layout = VIEWS_BASEDIR .$this->c . $l . '.php';
            include_once(__DIR__ . '/../../src/Blog/Model/Post.php');
            include_once(__DIR__ . '/../../src/Blog/Model/User.php');
            $this->post = new Post();
            $this->user = new User();
        }

        public function render($layout, $data)
        {
            $class = get_class($this);          //class Controller Post or Security or Json
            $classname = str_replace('Blog\\Controller\\', '', $class);
            $classname = str_replace('Controller', '', $classname);

            //View file
            $this->pathlayout = VIEWS_BASEDIR . $classname . '/' . $layout . '.php';
            //$this->posts = $data;
            //var_dump($data);
            foreach($data as $key => $a){
                //print_r($key . ' = ' .$a.' !');
                $this->posts[$key] = $a;
            }
            //var_dump($this->posts);
            $renderer = new Renderer();
            return new Response($renderer->render($this->pathlayout, $this->posts),$status='200');
        }

        function show($id)
        {
            $post = $this->getPost($id);
            $html = $this->render(VIEWS_BASEDIR . 'Post/show.html.php', array('post' => $post));
            return new Response($html);
        }

        public function getPost($p)
        {
        }

        function add()
        {
            return print_r('Abstract Controller Add </p>');
        }

        function redirect($url, $status = 302)
        {
            return new ResponseRedirect($url, $status);
        }

        function getAction($controller, $action)
        {
            // $this->getaction = $action;
            //echo '<br>This is Controller. We are get action - '. $action .'<br>';
            $c = new $controller;
            if (method_exists($c, $action)) {
                return $c->$action();
            } else {
                echo "<p>This action is not method at" . $controller . "</p>";
            }

        }

        public function getRequest()
        {
            return new Request();
        }

//********************** Service

        function generateRoute($name, $params)
        {

            $this->router = Service::get('router');
            return $this->router->buildRoute($name);
            //$router->buildRoute($name,$params);

        }
    }
}