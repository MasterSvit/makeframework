<?php
include_once(__DIR__.'/../framework/DI/Service.php');

class Loader //extends Singleton
{
    protected $pathfile;
    protected $namespacesMap = array();
    protected $addClass = array();
    public $p = '';
    //private $cName;
    public $def = array(
        "pattern" => '',
        "controller" => '',
        "action" => '');

    public function __construct()
    {
        spl_autoload_register('self::load');
    }

    public function load($class)
    {
        if(stripos($class,'Blog')) {
                if(!empty($this->namespacesMap[$class])){
                    require_once $this->namespacesMap[$class];
                    //echo '<p>Array was load!</p>';
                    if(class_exists($class)){echo 'Class download ' .$class.'<br>';}
                    //return true;
                }
                else{
                    echo '<p>Empty array!!!</p>';
                    return false;
                }
            }
            else{
                $className = str_replace('\\','/',$class);
                $className = str_replace('Framework/','',$className);
                $file = __DIR__.'/../framework/'.$className.'.php';
                $file = str_replace('//','/',$file);
                //echo '<p> This is '.$class . ' Class in '.$file.' </p>';
                if (is_readable($file)) {
                     include($file);
                     if(class_exists($class)){echo 'Class download ' .$class.'<br>';}
                     //       echo '<p> file path was found ' . $file . '</p>';
                     //break;
                }

            }
    }

    public function addNamespacePath($nameSpace,$path)
    {
        if (is_dir($path)) {
            $this->namespacesMap[$nameSpace] = $path;
            return true;
        }

        return false;
    }
}

$Loader = new Loader();
