<?php
//View
namespace Framework\Renderer {

    use Framework\Response\Response;

    class Renderer
    {
        protected $layout;
        protected $posts = array();
        protected $content = array();
        protected $l;
        protected $c;
        protected $po = array();

        public function __construct() {}

        // This is a data from Model
        public function render($main_index,$data)
        {
            //var_dump($data);
            $this->layout = $main_index;

            if(is_array($data) && !empty($data)){
                $this->posts = extract($data);
                $post = new \stdClass();
                ob_start();
                foreach($this->posts as $key=>$a){
                    for($i=0;$i<count($this->posts);$i++) {
                        $post->$key[$i] = $a;
                    }
                }

                include($this->layout);
                return ob_get_flush();
            }
            else {echo("Don't send Data");}
        }
    }
}