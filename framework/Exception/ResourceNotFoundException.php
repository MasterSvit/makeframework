<?php

namespace Exception {
    class ResourceNotFoundException extends Exception {

        public function __construct($mess,$code=0){
            parent::__construct($mess,0);
        }
    }
}