<?php
namespace Framework\Model {

    use Framework\DI\Service;

    abstract class ActiveRecord
    {
        public      $db2;
        public    static     $posts;
        protected   static   $table;
        protected   static   $sql;
        public   $id;
        public   $title;
        public          $content;
        public          $date;
        public  static  $where = '';
        public  static  $getSQL;
        public static $st;

        public function __construct(){
            //self::$table = Service::get('db');
            //self::$where = '';
            $this->db2 = Service::get('db');
            self::$sql = $this->db2;    //->query('SELECT name FROM posts'.self::$where);
        }

        protected function __clone() {}

        public static function find( $id ) {
	    $conn = self::$sql;
            if('all' == $id){
                $s = "SELECT * FROM posts";
                self::$st = $conn->prepare( $s );
                self::$st->execute();
                //var_dump(self::$st);
            }
            elseif(is_int($id)){
                $s = "SELECT * FROM posts WHERE id = :id";
                self::$st = $conn->prepare( $s );
                self::$st->bindValue( ":id", $id, \PDO::PARAM_INT );
                self::$st->execute();
                //var_dump(self::$st);
            }
	    self::$posts = self::$st->fetchAll();
	    $conn = null;
	    if ( !empty(self::$posts) ) return self::$posts;
	  }

//--------------------------

        public function save()
        {
            if ($this->id) {
                $sql = "UPDATE posts SET title = :title WHERE id = :id";
                $statement = $this->db->prepare($sql);
                $statement->bindParam("title", $this->title);
                $statement->bindParam("id", $this->id);
                $statement->execute();
            } else {
                $sql = "INSERT INTO posts (title) VALUES (:title)";
                $statement = $this->db->prepare($sql);
                $statement->bindParam("title", $this->title);
                $statement->execute();
                $this->id = $this->db->lastInsertId();
            }
        }
    }

}