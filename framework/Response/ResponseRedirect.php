<?php
namespace Framework\Response {

    class ResponseRedirect extends Response
    {

        function __construct($nameHeader, $status = 302)
        {
            header('Location: http://framework3/');
            exit;
        }
    }
}