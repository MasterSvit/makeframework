<?php

namespace Framework\Response {
//send code at response 200 or 500
    define('VIEWS_BASEDIR', __DIR__ . '/../../src/Blog/views/');
//    echo '<p>'.print_r(VIEWS_BASEDIR).'</p>';
    //use stdClass;

    interface ResponseInterface{
        public function setContent($c);
        public function getContent();
        /*public function setStatusCode($code,$text);
        public function getStatusCode();
        public function sendHeaders();*/
        public function sendContent();
        public function send();
        public function getType();
    }

    class Response implements ResponseInterface
    {
        protected $posts = array();
        protected $content = array();
        protected $postsArray = array();
        protected $statusText;
        protected $statusCode;
        public $template;
        protected $type;
        public $params;
        public static $statusTexts = array(
            200 => 'OK',
            302 => 'Moved Permanently',
            404 => 'Not Found',
            500 => 'Internal Server Error',
        );

        public function __construct($content, $status = '200')
        {
            $this->setContent($content);
            $this->setStatusCode($status);
            return $this;
        }

        /*function arrayToObject($array)
        {
            //var_dump($array);
            $post = new \stdClass();
            foreach($array as $key => $value){
                /*if(is_array($value)){
                    $value = $this->arrayToObject($value);
                }
                $post->$key = $value;
            }
            return $post;
        }*/

        public function setContent($content)
        {
            $this->posts = $content;
        }

        public function setStatusCode($code, $text = null)
        {
            $this->statusCode = $code;
            $code = (int)$code;
            if (null === $text) {
                $this->statusText = isset(self::$statusTexts[$code]) ? self::$statusTexts[$code] : '';
                return $this;
            }
            $this->statusText = $text;
            return $this;
        }

        public function getStatusCode()
        {
            return $this->statusCode;
        }

        public function getContent()
        {
            return $this->posts;
        }

        public function sendContent()
        {
            return $post = $this->posts;
            //return $this->posts = $this->arrayToObject($this->postsArray);
        }

        public function sendHeaders()
        {
            header(sprintf('HTTP 1.1 %d %s; Content-type: text/html', $this->statusCode, $this->statusText), true, $this->statusCode);
            return $this;
        }

        public function send()
        {
            $this->sendHeaders();
            $this->sendContent();
            return $this;
        }

        public function NotFound()
        {
            return 404 === $this->statusCode;
        }

        public function getType()
        {
            $this->type = 'text/html';
            return $this->type;
        }
    }
}