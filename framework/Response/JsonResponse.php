<?php
namespace Framework\Response {
//use \Framework\Response\Response;

class JsonResponse extends Response
{
    protected $content;
    protected $data = array();
    public $template;
    public $type;

    public function __construct($params){
        header(sprintf('HTTP 1.1 %d %s ; Content-type: application/json', $this->statusCode=200, $this->statusText), true, $this->statusCode);
        //header('Content-Type:'.$this->getType());
        $this->content = $params;
        $content = print_r('<h3>'.$params['body'].'</h3>');
        include_once(VIEWS_BASEDIR . 'layout.html.php');
        return $content;
    }

    public function getType(){
        $this->type = 'application/json';
        return $this->type;
    }
}
}