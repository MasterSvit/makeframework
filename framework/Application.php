<?php
    use Framework\DI\Service;
    use Framework\Request\Request;
    use Framework\Router\Router;
    use Framework\Response\Response;
    use Framework\Response\ResponseInterface;
    use Framework\Controller\Controller;
    use Framework\Renderer\Renderer;
    use Framework\Security\Security;

    class Application
    {
        private $config; //configuration
        private $Requestclass; // Class request
        private $Routerclass; // Class router
        private $Controllerclass; //Class controller
        private $Responseclass; //Class response
        public $db;
        public $service;
        public $ss;
        public $security;


    public function __construct($aconfig)
        {
            $this->config = include($aconfig);
            try {
                $this->db = new \PDO($this->config['pdo']['dns'],$this->config['pdo']['user'],$this->config['pdo']['password']);
                /* foreach($this->db->query('SELECT * from posts') as $row) {
                     echo '<table border="1"><tr>';
                     for($i=0;$i<count($row);$i++){
                         print_r('<td>'.$row[$i].'</td>');
                     }
                     echo '</tr></table>';
                 }*/

                Service::set('db', $this->db);
                Service::set('router', $this->Routerclass);
                //Service::set('security',$this->security);
                //Service:set('session',$this->ss);
            }
            catch (\PDOException $e) {
                print($e->getMessage());
            }
            $this->Requestclass = new Request();
            $this->Routerclass = new Router();
            $this->Controllerclass = new Controller();
            $this->Responseclass = new Response(NULL);
            $this->security = new Security();


        }

        function getModel($name) {
            $modelFile = __DIR__ . '/../../src/Blog/Model/' . $name . '.php';
            if ( file_exists($modelFile) ) {
                include($modelFile);
            }
        }

        public function run()
        {
            //this is a difference at dir script (web) and dir-url-request
            //echo 'Dir = ' . dirname($_SERVER["PHP_SELF"]) . ' Request = ' . $_SERVER["REQUEST_URI"] . '<br>';
            $cDefault = __DIR__ . '/../src/Blog/Controller/PostController.php';
            $pattern = $this->Requestclass->getURI(dirname($_SERVER["PHP_SELF"]), $_SERVER["REQUEST_URI"]);
            print_r($pattern);
            //print_r($this->config['routes']);
            $searchRoute = $this->Routerclass->route($this->config['routes'], $pattern);
            // try {
                if (!empty($searchRoute)) {
                    $cFile = __DIR__ . '/../src/' . str_replace('\\', '/', $searchRoute['controller']) . '.php';
                    //Files with Model
                    if(stripos($searchRoute['controller'],'post') <> NULL){
                        $modelFile = __DIR__.'/../../src/Blog/Model/Post.php';
                    }
                    elseif(stripos($searchRoute['controller'],'security') <> NULL){
                        $modelFile = __DIR__.'/../../src/Blog/Model/User.php';
                    }
                    else {$modelFile = NULL;}

                    if (file_exists($cFile)) {
                        if (file_exists($modelFile)) {
                            include_once $modelFile;
                        };
                        include_once($cFile);

                        $reflection = new ReflectionClass($searchRoute['controller']);
                        $actionName = $searchRoute['action'] . 'Action';
                        $action = $reflection->getMethod($actionName);
                        $arr = $action->invokeArgs(new $searchRoute['controller'], $action->getParameters());

                        $response = $arr;
                        //}              // End of reflection hasMethod
                        if('TestController' == $searchRoute['controller']){$response->send();}
                        else{
                        if ($response instanceof ResponseInterface) {
                             $layout = __DIR__.'/../src/Blog/views/layout.html.php';
                            if(file_exists($layout)){
                                $render = new Renderer();
                                $wrapper = $render->render($layout, array('content' => $response->getContent()));
                                $response = new Response($wrapper);
                                //} else {
                                //throw BadResponseException('This is not right response');
                                //}
                                $response->send();
                            }
                        else{echo '<p>I don\'t have this file<p>';}
                        }
                        //}   //echo '<p>File was include' . $cFile. '</p>';
                        //}
                    }
                    }
                        else {
                        //echo '<p>' . $cFile . '</p>';
                        include($cDefault);
                        //echo '<p>don\'t say Good bye</p>';
                    }

                }
                /*
                            //catch (Exception\ResourceNotFoundException $e) {
                            //        $response = new Response('N Found', 404);
                            //}/* catch (Exception\ResourceNotFoundException $e) {
                                    $response = new Response('An error occurred', 500);
                            }*/

                //   else{ echo '<br>Don\'t found method!<br>';}
            //}
        }
}
